---
title: Programme
slug: programme
description: null
draft: false
menu:
  main:
    name: Programme
    weight: 999
url: /programme/
---

# 1er rencontre à Rennes (2022)

(*ce programme est susceptible aux changements pendant les prochains jours*)

*Date :* 7 et 8 décembre

*Lieu :* Inria de l'université de Rennes

## Jour 1
    - 10h00-10h30: Accueil
    - 10h30-12h00: Tour de table et présentation des activités *Santé Numérique (SN)* par centre.
    - 12h00-14h00: Déjeuner
    - 14h00-15h45: Présentations de projets, partie 1.
    - 15h45-16h00: Pause café.
    - 16h00-17h45: Présentations de projets, partie 2.

## Jour 2
    - 08h00-08h15: Accueil
    - 08h15-10h15: Session de travail 1, **Gestion de données**
    - 10h15-10h30: Pause café
    - 10h30-12h30: Session de travail 2, **Chaine de traitement pour le dev. logiciel**

### Précisions du programme :

- Présentation des activités SN par centre : Lyon (Antoine), Rennes (Florent), Bordeaux (Dan), Paris (Mauricio).
- Présentation de projets (15 min par projet). Voir liste de points à être abordé. Liste de projets:
    - Bdx (PrimeTimeIRE,Vocapy,TBD)
    - Paris  (Clinica, ClinicaDL, MedKit)
    - Rennes (medInria, Shanoir, OpenVibe, NaviScope, BioImageIT)
    - Lyon ()
    - Sophia (Fed-BioMed)
- Session du travail 1, **Gestion de données** :
    - Formats: e.g., en imagerie DICOM/BIDS
    - Accès: PACS, Shanoir
    - Problèmatiques autour des fournisseurs de données : EDS, HDH, SNDS, données sensibles. Proposition de solutions à Inria.
    - Systèmes de contôle de version pour les données.
    - Anonimisation, pseudonimisation

- Session de travail 2, **Chaine de traitement pour le dev. logiciel**
    - Comment les données sont intégrées à la chaîne de traitement (cas spécial pour le traitement féderé) ?
    - Déploiement de résultats (données, modèles, documentation, tutoriaux).
    - Tests, CI/CD, etc..
